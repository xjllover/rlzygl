package edu.taru.hr.service;

import java.util.Vector;

import edu.taru.hr.dao.UserDaoImpl;
import edu.taru.hr.pojo.User;

/*
 * 业务层（服务端）
 */
public class UserServiceImpl {
UserDaoImpl daoImpl =new UserDaoImpl();
/*
 * 注册
 */
public void register(User user){
	daoImpl.register(user);
}
/*
 * 查询
 */
public User select(String id){
	 return daoImpl.selectVector(id);

}
/*
 * 模糊查询
 */
public Vector<Vector<String>>  select2(String A){
	return daoImpl.select2(A);

}
/*
 * 删除
 */
public void delete(String id ){
	daoImpl.delect(id);
}
/*
 * 更新
 */
public void update(User user){
	 daoImpl.update(user);
}
}

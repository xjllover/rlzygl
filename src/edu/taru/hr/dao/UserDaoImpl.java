package edu.taru.hr.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import edu.taru.hr.pojo.User;
import edu.taru.hr.util.SqlHelper;

public class UserDaoImpl {
	/*
	 * 插入
	 */
public void register(User user){
	String sql="insert into User  values (uuid(),?,?,?,?,?,?,?)";
	SqlHelper.update(sql,user.getName(),user.getUsername(),user.getPassword(),user.getCity(),user.getEmail(),user.getPhone(),user.getSex());
}
/*
 * 删除
 */
public void delect(String id){
	String sql="delete from user where id=?";
	SqlHelper.update(sql,id);
}
/*
 * 更新
 */
public void update(User user){
	String sql="update user set name= ?, username = ?, password=?,city=?,email =?, phone =?, sex =? where id=?";
	SqlHelper.update(sql,user.getUsername(),user.getPassword(),user.getId());
}
/**
 * 查询
 * @return
 */
public User selectVector(String id){
	String sql="select * from user where id=?";
	User user=null;
	List<HashMap<String ,Object>> list =SqlHelper.select2(sql,id) ;//一条数据
	if(list!=null&&list.size()>0){
		user=new User();
		HashMap<String ,Object> map=list.get(0);
		user.setId(map.get("id")==null?null:map.get("id").toString());
		user.setName(map.get("name")==null?null:map.get("name").toString());
		user.setUsername(map.get("username")==null?null:map.get("username").toString());
		user.setPassword(map.get("password")==null?null:map.get("password").toString());
		user.setCity(map.get("city")==null?null:map.get("city").toString());
		user.setEmail(map.get("email")==null?null:map.get("email").toString());
		user.setPhone(map.get("phone")==null?null:map.get("phone").toString());		
		user.setSex(map.get("sex")==null?null:map.get("sex").toString());		
	}
	return user;

}
/**
 * 模糊查询
 */
public Vector<Vector<String>> select2(String A){
	String sql="select * from user where id=? or name like ? escape '*' ";
	List<HashMap<String ,Object>> list =SqlHelper.select2(sql,A,"%"+A+"%") ;
	Vector<Vector<String>> v=new Vector<Vector<String>>();
	if(list!=null&&list.size()>0){
		for(HashMap<String ,Object> map:list){
		Vector<String> vector=new Vector<String>();
		User user=new User();
		user.setId(map.get("id")==null?null:map.get("id").toString());
		user.setName(map.get("name")==null?null:map.get("name").toString());
		user.setPassword(map.get("password")==null?null:map.get("password").toString());
		user.setCity(map.get("city")==null?null:map.get("city").toString());
		user.setEmail(map.get("email")==null?null:map.get("email").toString());
		user.setPhone(map.get("phone")==null?null:map.get("phone").toString());		
		user.setSex(map.get("sex")==null?null:map.get("sex").toString());		
		vector.add(user.getId());
		vector.add(user.getName());
		vector.add(user.getPassword());
		vector.add(user.getCity());
		vector.add(user.getEmail());
		vector.add(user.getPhone());
		vector.add(user.getSex());
		v.add(vector);
		}
	}
	return v;

}

}

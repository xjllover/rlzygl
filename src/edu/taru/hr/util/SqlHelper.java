package edu.taru.hr.util;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import edu.taru.hr.pojo.User;

public class SqlHelper {
	private static final String url="url";
	private static final String user="user";
	private static final String password="password";
	private static final String driver="driver";
	 private static ThreadLocal<Connection> local=new ThreadLocal<Connection>();//本地变量
	 private static Properties pro =new Properties();
	 /*
	  * 加载配置文件
	  */
	 static{
		 try {
			pro.load(SqlHelper.class.getClassLoader().getResourceAsStream("jdbc.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	 }
	/*
	 * 加载驱动
	 */
	 static{
			try {
				Class.forName(pro.getProperty(driver));//获取配置文件中的driver信息
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
	 } 
	/*
	 * 打开连接
	 */
	public static Connection openConnection(){
	Connection conn=local.get();
	try {
		if(conn==null || conn.isClosed()){
			//获取配置文件中的信息
			conn =DriverManager.getConnection(pro.getProperty(url),pro.getProperty(user),pro.getProperty(password));
			local.set(conn);
		}
	} catch (SQLException e) {
		e.printStackTrace();
	}
	return conn;
}
	/*
	 * 查询
	 */
	public static List<HashMap<String,Object>> select2 (String sql,Object ...params){
		Connection conn =SqlHelper.openConnection();
		ResultSet rs=null;
		PreparedStatement pst=null;
		 List<HashMap<String,Object>>  rows=new  ArrayList<HashMap<String,Object>> ();
		try {
			pst=conn.prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(params!=null){
			for(int i=0;i<params.length;i++){
				try {
					pst.setObject(i+1, params[i]);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		try {
			rs=pst.executeQuery();
			ResultSetMetaData rsmd=rs.getMetaData();//获取列头元数据
			int length =rsmd.getColumnCount();//获取列的数量
			while(rs.next()){
				HashMap<String ,Object> map=new HashMap<String ,Object>();
				for(int i=0;i<length;i++){
					String colum=rsmd.getColumnLabel(i+1);//得到列名
					map.put(colum ,rs.getObject(colum));//根据标签名 获取rs中的数据
				}
				rows.add(map);//添加的是HashMap的数据
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("执行异常",e);
		}finally{
			if(rs!=null){
				try {
					rs.close();
				} catch (SQLException e) {
					
					e.printStackTrace();
				}
			}
			if(pst!=null){
				try {
					pst.close();
				} catch (SQLException e) {
		
					e.printStackTrace();
				}
			}
		}
		return rows;
		
	}
	/*
	 * 更新
	 */
	public static  List<HashMap<String,Object>> update(String sql, Object... obj) {
		 List<HashMap<String,Object>>  rows=new  ArrayList<HashMap<String,Object>> ();
		PreparedStatement pst = null;
		try {
			pst = SqlHelper.openConnection().prepareStatement(sql);
			if (obj != null) {
				for (int i = 0; i < obj.length; i++) {
					pst.setObject(i + 1, obj[i]);
				}
				int j = pst.executeUpdate();
					HashMap<String ,Object> map=new HashMap<String ,Object>();
					rows.add(map);//添加的是HashMap的数据
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("更新异常", e);
		} finally {
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return rows;
	}
	/*
	 * 关闭链接
	 */
	private static void close(){
		Connection conn=local.get();
		if(conn!=null){
			try {
				conn.close();
				local.remove();
				conn=null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	/*
	 * 接口
	 */
	public static interface RowHandlerMapper<T>{
		public List<T>mapping(ResultSet rs);

		List<User> Maping(ResultSet rs);
	}
}

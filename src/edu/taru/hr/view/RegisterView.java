package edu.taru.hr.view;

import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import java.awt.BorderLayout;

import javax.swing.JPasswordField;

import edu.taru.hr.pojo.User;
import edu.taru.hr.service.UserServiceImpl;

import java.awt.event.ActionListener;
import javax.swing.JRadioButton;

public class RegisterView extends JFrame {
	private JTextField textField;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JPasswordField passwordField;
	private JTextField textField_1;
	private JTextField textField_5;

	      
	    public RegisterView()  
	    {  
	        //初始化组件  
	      
	        this.setTitle("注册界面");  
	        this.setBounds(200, 100, 400, 370);  
	        
	        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
	        getContentPane().setLayout(null);
	        
	        JLabel label = new JLabel("\u7528\u6237\u540D");
	        label.setBounds(28, 80, 54, 15);
	        getContentPane().add(label);
	        
	        textField = new JTextField();
	        textField.setBounds(125, 77, 173, 21);
	        getContentPane().add(textField);
	        textField.setColumns(10);
	        
	        JLabel lblNewLabel = new JLabel("\u5BC6\u7801");
	        lblNewLabel.setBounds(28, 111, 54, 15);
	        getContentPane().add(lblNewLabel);
	        
	        JLabel lblNewLabel_1 = new JLabel("\u59D3\u540D");
	        lblNewLabel_1.setBounds(28, 142, 54, 15);
	        getContentPane().add(lblNewLabel_1);
	        
	        JLabel lblNewLabel_2 = new JLabel("\u90AE\u7BB1");
	        lblNewLabel_2.setBounds(28, 173, 54, 15);
	        getContentPane().add(lblNewLabel_2);
	        
	        JLabel lblNewLabel_3 = new JLabel("\u7535\u8BDD");
	        lblNewLabel_3.setBounds(28, 209, 54, 15);
	        getContentPane().add(lblNewLabel_3);
	        
	        textField_2 = new JTextField();
	        textField_2.setBounds(125, 139, 173, 21);
	        getContentPane().add(textField_2);
	        textField_2.setColumns(10);
	        
	        textField_3 = new JTextField();
	        textField_3.setBounds(125, 170, 173, 21);
	        getContentPane().add(textField_3);
	        textField_3.setColumns(10);
	        
	        textField_4 = new JTextField();
	        textField_4.setBounds(125, 206, 173, 21);
	        getContentPane().add(textField_4);
	        textField_4.setColumns(10);
	        
	        JButton btnNewButton = new JButton("\u6CE8\u518C");
	        btnNewButton.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		
	        		String name=textField_2.getText();
	        		String username=textField.getText();
	        		String password=new String(passwordField.getPassword());
	        		String email=textField_3.getText();
	        		String phone=textField_4.getText();	
	        		String id=textField_1.getText();
	        		String city =textField_5.getText();
	        		
	        		// 获得用户名
					User users = new User();
					users.setName(name);
					users.setPassword(password);
					users.setUsername(username);
					users.setEmail(email);
					users.setPhone(phone);
					users.setId(id);
					users.setCity(city);
					if (name.isEmpty()) {
						JOptionPane.showMessageDialog(RegisterView.this, "注册失败！");
					} else {
						UserServiceImpl userServiceImpl = new UserServiceImpl();
						try {
							userServiceImpl.register(users);
							LoginView login=new LoginView();
							RegisterView.this.dispose();
						} catch (Exception e1) {
							JOptionPane.showMessageDialog(RegisterView.this,"注册成功！");
						}
						
					}

	        	}
	        });
	        btnNewButton.setBounds(10, 299, 93, 23);
	        getContentPane().add(btnNewButton);
	        
	        JButton btnNewButton_1 = new JButton("\u8FD4\u56DE");
	        btnNewButton_1.setBounds(132, 299, 93, 23);
	        getContentPane().add(btnNewButton_1);
	        
	        passwordField = new JPasswordField();
	        passwordField.setBounds(125, 108, 173, 21);
	        getContentPane().add(passwordField);
	        
	        JButton btnNewButton_2 = new JButton("\u91CD\u7F6E");
	        btnNewButton_2.setBounds(262, 299, 93, 23);
	        getContentPane().add(btnNewButton_2);
	        
	        JLabel lblNewLabel_4 = new JLabel("\u7F16\u53F7");
	        lblNewLabel_4.setBounds(28, 41, 54, 15);
	        getContentPane().add(lblNewLabel_4);
	        
	        textField_1 = new JTextField();
	        textField_1.setBounds(125, 38, 173, 21);
	        getContentPane().add(textField_1);
	        textField_1.setColumns(10);
	        
	        JLabel lblNewLabel_5 = new JLabel("\u57CE\u5E02");
	        lblNewLabel_5.setBounds(28, 240, 54, 15);
	        getContentPane().add(lblNewLabel_5);
	        
	        textField_5 = new JTextField();
	        textField_5.setText("");
	        textField_5.setBounds(125, 237, 173, 21);
	        getContentPane().add(textField_5);
	        textField_5.setColumns(10);
	        this.setVisible(true); 
	    }  
	    public void actionPerformed(ActionEvent e) {  
	        
	        if(e.getActionCommand()=="返回")  
	        {  
	            this.dispose();  
	            new LoginView();  
	              
	        } 
	          
	    } 
public static void main(String[] args) {
	RegisterView rv=new RegisterView();
}
}

package edu.taru.hr.view;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;

import java.awt.Color;
import java.awt.Font;

import javax.swing.ImageIcon;

import edu.taru.hr.pojo.User;
import edu.taru.hr.service.LoginServiceImpl;

public class LoginView extends JFrame{

public static void main(String[] args) {
	LoginView t=new LoginView();
	}
	JButton loginbut =new JButton("登录");
	JButton exitbut =new JButton("退出");
	JTextField loginText =new JTextField();
	JTextField passwdText =new JTextField();
	JLabel jl=new JLabel("用户名");
	JLabel jl2=new JLabel("密码");
	private final JMenuBar menuBar = new JMenuBar();
	private final JMenuItem menuItem_1 = new JMenuItem("\u7528\u6237\u4FE1\u606F");
	private final JSeparator separator = new JSeparator();
	private final JMenuItem menuItem_2 = new JMenuItem("\u9000\u51FA");
	private final JMenuItem menuItem_3 = new JMenuItem("\u67E5\u8BE2");
	private final JMenuItem menuItem_4 = new JMenuItem("\u66FF\u6362");
	private JPasswordField passwordField;
	//添加事件
public LoginView(){
	this.setTitle("人力资源管理");
	getContentPane().setLayout(null);
	this.setVisible(true);
	this.setLocationRelativeTo(null);
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	this.setSize(400,400);
	loginbut.setBounds(300,200,70,35);
	getContentPane().add(loginbut);
	exitbut.setBounds(300,250,70,35);
	getContentPane().add(exitbut);
	loginText.setBounds(100,200,150,35);
	getContentPane().add(loginText);
	jl.setBounds(50,200,40,35);
	getContentPane().add(jl);
	jl2.setBounds(50,250,43,35);
	getContentPane().add(jl2);
	
	JLabel lblNewLabel = new JLabel("\u6B22\u8FCE\u4F7F\u7528");
	lblNewLabel.setFont(new Font("华文新魏", Font.PLAIN, 40));
	lblNewLabel.setIcon(new ImageIcon("D:\\PS\u56FE\u5E93\\\u58C1\u7EB8\u5927\u89C2\\\u52A8\u7269\u4E16\u754C\\6.JPG"));
	lblNewLabel.setBounds(37, -2, 321, 192);
	getContentPane().add(lblNewLabel);
	
	passwordField = new JPasswordField();
	passwordField.setBounds(100, 257, 150, 21);
	getContentPane().add(passwordField);
	
	setJMenuBar(menuBar);
	
	JMenu menu = new JMenu("\u7528\u6237\u7BA1\u7406");
	menuBar.add(menu);
	
	JMenuItem menuItem = new JMenuItem("\u5207\u6362\u7528\u6237");
	menu.add(menuItem);
	
	menu.add(menuItem_1);
	
	menu.add(separator);
	
	menu.add(menuItem_2);
	
	JMenu menu_1 = new JMenu("\u6587\u4EF6\u7BA1\u7406");
	menuBar.add(menu_1);
	
	menu_1.add(menuItem_3);
	
	menu_1.add(menuItem_4);
	
	exitbut.addActionListener(new ActionListener(){

		public void actionPerformed(ActionEvent arg0) {
			System.exit(0);
		}
	});
	//登陆事件
	loginbut.addActionListener(new ActionListener(){

		public void actionPerformed(ActionEvent a) {
		//获得用户名
			String name =loginText.getText();
			//获得密码
			String passwd=new String(passwordField.getPassword());
			LoginServiceImpl loginServiceImpl =new LoginServiceImpl();
			try {
				User user =loginServiceImpl.login(name, passwd);
				
				if(user==null){
					JOptionPane.showMessageDialog(LoginView.this, "用户名或密码错误");
				}else{
					MainView main=new MainView();
					LoginView.this.dispose();
					JOptionPane.showMessageDialog(LoginView.this, "登陆成功");
					
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(LoginView.this, "登陆异常");
			}
		}
	});
	//退出事件
			exitbut.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					System.exit(0);
				}
			});
			
}
}

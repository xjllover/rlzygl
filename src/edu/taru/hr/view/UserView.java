package edu.taru.hr.view;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import edu.taru.hr.pojo.User;
import edu.taru.hr.service.UserServiceImpl;

public class UserView extends JFrame {
	private JTextField textField;
	String []  header ={"id","name","username","password","city","email","phone","sex"};
	String [][] data =new  String [8][8];
	public static User user;
	private JTable 	table = new JTable(data,header);
	
	public UserView() {
		getContentPane().setLayout(null);
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "\u641C\u7D20\u9762\u677F", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(0, 21, 514, 98);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel label = new JLabel(" \u7F16\u53F7");
		label.setBounds(32, 35, 54, 15);
		panel.add(label);
		
		textField = new JTextField();
		textField.setBounds(133, 32, 158, 21);
		panel.add(textField);
		textField.setColumns(10);
		String s=textField.getSelectedText();
		JButton button = new JButton("\u641C\u7D22");
		button.addActionListener(new ActionListener() {
			//搜索按钮事件
			public void actionPerformed(ActionEvent a) {
				UserServiceImpl service  =new UserServiceImpl();
				Vector<Vector<String>>   vectors =service.select2(textField.getText());
				//把头转换成Vector
				Vector<String>   new_header =new Vector<String>();
				for(int i =0;i<header.length;i++){
					new_header.add(header[i]);
				}
					//构建一个表格模型 
					DefaultTableModel    tablemodel =new  DefaultTableModel(vectors,new_header);
					table.setModel(tablemodel);
					//更新
					table.updateUI();
			}
		});
		button.setBounds(311, 31, 93, 23);
		panel.add(button);
		/*
		 * 刷新
		 */
		JButton button_1 = new JButton("\u5237\u65B0");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UserServiceImpl service  =new UserServiceImpl();
				Vector<Vector<String>>   vectors =service.select2(textField.getText());
				//把头转换成Vector
				Vector<String>   new_header =new Vector<String>();
				for(int i =0;i<header.length;i++){
					new_header.add(header[i]);
				}
					//构建一个表格模型 
					DefaultTableModel    tablemodel =new  DefaultTableModel(vectors,new_header);
					table.setModel(tablemodel);
					//更新
					table.updateUI();
				
			}
		});
		button_1.setBounds(311, 64, 93, 23);
		panel.add(button_1);
		/*
		 * 删除
		 */
		JButton btnNewButton = new JButton("\u5220\u9664");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UserServiceImpl service  =new UserServiceImpl();				
				  service.delete(textField.getText());			
					//更新
					table.updateUI();
				
			}
		});
		btnNewButton.setBounds(414, 31, 93, 23);
		panel.add(btnNewButton);
		/*
		 * 修改
		 */
		JButton btnNewButton_1 = new JButton("\u4FEE\u6539");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int index=table.getSelectedRow();
				String id=table.getValueAt(index, 0).toString();
				user.setName(table.getValueAt(index, 1).toString());
				user.setUsername(table.getValueAt(index, 2).toString());
				user.setPassword(table.getValueAt(index, 3).toString());
				user.setCity(table.getValueAt(index, 4).toString());
				user.setEmail(table.getValueAt(index, 5).toString());
				user.setPhone(table.getValueAt(index, 6).toString());
				UserServiceImpl userServiceImpl=new UserServiceImpl();
				User user =userServiceImpl.select(id);
				UpdateView update =new UpdateView();
				
			}
		});
		btnNewButton_1.setBounds(411, 64, 93, 23);
		panel.add(btnNewButton_1);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "\u6570\u636E\u533A\u57DF", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(0, 129, 514, 223);
		getContentPane().add(panel_1);
		panel_1.setLayout(new BorderLayout(0, 0));
		this.setResizable(false);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JScrollPane scrollPane = new JScrollPane();
		panel_1.add(scrollPane, BorderLayout.CENTER);
		
	
		scrollPane.setViewportView(table);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(200, 362, 2, 2);
		getContentPane().add(scrollPane_1);
		
		
	}
}

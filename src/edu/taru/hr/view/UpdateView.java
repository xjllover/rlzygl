package edu.taru.hr.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import edu.taru.hr.pojo.User;

public class UpdateView  extends JFrame{
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JPasswordField passwordField;
	private JTextField textField_2;
	User users = new User();
	public UpdateView() {
		getContentPane().setLayout(null);
		
		JLabel label = new JLabel("\u7F16\u53F7");
		label.setBounds(32, 44, 54, 15);
		getContentPane().add(label);
		
		JLabel label_1 = new JLabel("\u7528\u6237\u540D");
		label_1.setBounds(32, 84, 54, 15);
		getContentPane().add(label_1);
		
		JLabel label_2 = new JLabel("\u5BC6\u7801");
		label_2.setBounds(32, 123, 54, 15);
		getContentPane().add(label_2);
		
		JLabel label_3 = new JLabel("\u59D3\u540D");
		label_3.setBounds(32, 166, 54, 15);
		getContentPane().add(label_3);
		
		JLabel label_4 = new JLabel("\u90AE\u7BB1");
		label_4.setBounds(32, 202, 54, 15);
		getContentPane().add(label_4);
		
		JLabel label_5 = new JLabel("\u7535\u8BDD");
		label_5.setBounds(32, 239, 54, 15);
		getContentPane().add(label_5);
		
		textField = new JTextField();
		//回显
		textField.setText(users.getId());
		textField.setBounds(92, 41, 142, 21);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(92, 81, 142, 21);
		getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(92, 163, 142, 21);
		getContentPane().add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(92, 199, 142, 21);
		getContentPane().add(textField_4);
		textField_4.setColumns(10);
		
		textField_5 = new JTextField();
		textField_5.setBounds(92, 236, 142, 21);
		getContentPane().add(textField_5);
		textField_5.setColumns(10);
		/*
		 * 修改
		 */
		JButton button = new JButton("\u4FEE\u6539");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name=textField_3.getText();
        		String username=textField_1.getText();
        		String password=new String(passwordField.getPassword());
        		String email=textField_4.getText();
        		String phone=textField_5.getText();	
        		String id=textField.getText();
        		String city=textField_2.getText();	
        		// 获得用户名
				UserView.user.setName(name);
				UserView.user.setPassword(password);
				UserView.user.setUsername(username);
				UserView.user.setEmail(email);
				UserView.user.setPhone(phone);
				UserView.user.setId(id);
				UserView.user.setCity(city);
				
			}
		});
		button.setBounds(305, 260, 93, 23);
		getContentPane().add(button);
		/*
		 * 返回
		 */
		JButton btnNewButton = new JButton("\u8FD4\u56DE");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UserView u=new UserView();
				UpdateView.this.dispose();
			}
		});
		btnNewButton.setBounds(305, 291, 93, 23);
		getContentPane().add(btnNewButton);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(92, 120, 142, 21);
		getContentPane().add(passwordField);
		
		JLabel label_6 = new JLabel("\u57CE\u5E02");
		label_6.setBounds(32, 264, 54, 15);
		getContentPane().add(label_6);
		
		JLabel label_7 = new JLabel("\u6027\u522B");
		label_7.setBounds(32, 295, 54, 15);
		getContentPane().add(label_7);
		
		textField_2 = new JTextField();
		textField_2.setBounds(92, 261, 142, 21);
		getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		JRadioButton radioButton = new JRadioButton("\u7537");
		radioButton.setBounds(92, 291, 59, 23);
		getContentPane().add(radioButton);
		
		JRadioButton radioButton_1 = new JRadioButton("\u5973");
		radioButton_1.setBounds(165, 291, 78, 23);
		getContentPane().add(radioButton_1);
		this.setVisible(true);
		this.setSize(535, 424);
	}
}

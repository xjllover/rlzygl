package edu.taru.hr.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JPanel;

public class MainView  extends JFrame {
	public static void main(String[] args) {
		MainView m=new MainView();
	}
	

	JMenuBar menuBar = new JMenuBar();
	public MainView() {
		setJMenuBar(menuBar);
		this.setTitle("爱美网");
		JMenu menu = new JMenu("\u8BBE\u7F6E");
		menuBar.add(menu);
		this.setVisible(true);
		this.setSize(500, 600);
		//切换用户
		JMenuItem menuItem = new JMenuItem("\u5207\u6362\u7528\u6237");
		menu.add(menuItem);
		
		//个人信息
		JMenuItem menuItem_1 = new JMenuItem("\u4E2A\u4EBA\u4FE1\u606F");
		menu.add(menuItem_1);
		//修改密码
		JMenuItem menuItem_2 = new JMenuItem("\u4FEE\u6539\u5BC6\u7801");
		menu.add(menuItem_2);
		//分割线
		JSeparator separator = new JSeparator();
		menu.add(separator);
		//退出
		JMenuItem menuItem_3 = new JMenuItem("\u9000\u51FA");
		menu.add(menuItem_3);
		
		JMenu menu_1 = new JMenu("\u7528\u6237\u7BA1\u7406");
		menuBar.add(menu_1);
		
		JMenuItem menuItem_4 = new JMenuItem("\u6CE8\u518C\u7528\u6237");
		menu_1.add(menuItem_4);
		
		JMenuItem menuItem_5 = new JMenuItem("\u7528\u6237\u5217\u8868");
		//用户列表查询
		menuItem_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UserView   view =new UserView();
				view.setSize(800, 600);
				view.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				view.setVisible(true);
				view.setLocationRelativeTo(null);
				
			}
		});
		menu_1.add(menuItem_5);
		
		JMenu menu_2 = new JMenu("\u8D44\u6E90\u7BA1\u7406");
		menuBar.add(menu_2);
		
		JMenuItem menuItem_6 = new JMenuItem("\u65B0\u589E\u8D44\u6E90");
		menu_2.add(menuItem_6);
		
		JMenuItem menuItem_7 = new JMenuItem("\u67E5\u8BE2\u6240\u6709\u8D44\u6E90");
		menu_2.add(menuItem_7);
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		//切换用户事件按钮
		menuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				LoginView LoginView =new LoginView();
				LoginView.setSize(470,300);
				LoginView.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				LoginView.setVisible(true);
				LoginView.setLocationRelativeTo(null);
				MainView.this.dispose();
			}
		});
		JLabel lblNewLabel = new JLabel("\u7F51\u7F57\u5929\u4E0B\u7F8E\u5973\uFF0C\u641C\u96C6\u4E16\u754C\u540D\u6A21\uFF0C\u4E0D\u6015\u4F60\u4E0D\u77E5\u9053\uFF0C\u5C31\u6015\u4F60\u4E0D\u6562\u6765\uFF01");
		lblNewLabel.setFont(new Font("楷体", Font.BOLD | Font.ITALIC, 30));
		getContentPane().add(lblNewLabel, BorderLayout.NORTH);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon("C:\\Users\\xjl\\Pictures\\Saved Pictures\\\u96C5\\1  (1).jpg"));
		getContentPane().add(lblNewLabel_1, BorderLayout.WEST);
		
		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setIcon(new ImageIcon("C:\\Users\\xjl\\Desktop\\3afd4ec3673c226367ee789bbfa0d6ae_t01f9f6958f01143df6.jpg"));
		getContentPane().add(lblNewLabel_2, BorderLayout.CENTER);
		
		JLabel lblNewLabel_3 = new JLabel("");
		lblNewLabel_3.setIcon(new ImageIcon("C:\\Users\\xjl\\Pictures\\Saved Pictures\\\u96C5\\1  (4).jpeg"));
		getContentPane().add(lblNewLabel_3, BorderLayout.EAST);
		
		JLabel label = new JLabel("\u4F60\u8FD8\u5728\u7B49\u4EC0\u4E48\uFF01\u4E00\u76F4\u5728\u7B49\u4F60\u5462\uFF01");
		label.setFont(new Font("宋体", Font.ITALIC, 15));
		getContentPane().add(label, BorderLayout.SOUTH);
		
		//退出选项添加监听事件
		menuItem_3.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				LoginView LoginView =new LoginView();
				LoginView.setSize(470,300);
				LoginView.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				LoginView.setVisible(true);
				LoginView.setLocationRelativeTo(null);
				MainView.this.dispose();
			}
		});
		
		//注册用户事件，显示注册界面
		menuItem_4.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				RegisterView registerView =new  RegisterView();
				registerView.setSize(800, 600);
				registerView.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				registerView.setVisible(true);
				registerView.setLocationRelativeTo(null);
				
			}
		});

	}

}
